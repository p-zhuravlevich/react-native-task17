const express = require("express");
const authController = require("../controllers/authController");
const authRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});

authRouter.post("/register", urlencodedParser, authController.regUser);
authRouter.post("/login", urlencodedParser, authController.loginUser);
 
module.exports = authRouter;