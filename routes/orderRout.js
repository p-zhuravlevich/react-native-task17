const express = require("express");
const orderController = require("../controllers/orderController.js");
const orderRout = express.Router();
const auth = require ("../auth.js");

orderRout.post("/create", auth.verifyToken, orderController.createOrder);
orderRout.delete("/:id", auth.verifyToken, orderController.deleteOrder);
orderRout.get("/get", auth.verifyToken, orderController.getAllOrders);
orderRout.get("/:id", auth.verifyToken, orderController.getOrderById);

module.exports = orderRout;
