const User = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');
const Cart = require('../models/cart.js');


const regUser = async (req,res) => {
    try {
        const { name, login, password } = req.body;
    // if (!(login && password && name)) {
    //   res.status(401).send("All fields is required.");
    // }
    const oldUser = await User.findOne({ login });
    if (oldUser) {
      return res.status(409).send("User already exist.");
    }
    role = req.body.role;
    encryptedPassword = await bcrypt.hash(password, 5);
    const user = await User.create({
      name,
      login: login.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const newCart = await new Cart({});
            newCart.user = user._id;
            await newCart.save();
            user.cart = newCart._id;
            await user.save();
    const token = jwt.sign(
      { user_id: user._id, login },
        accessToken,
      {
        expiresIn: "22h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    res.status(400).send("Broken")
  }
};


const loginUser = async (req, res) => {
  try {
    const { login, password } = req.body;
    if (!(login && password)) {
      res.status(400).send("All input is required");
    }
    const user = await User.findOne({ login });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, login },
          accessToken,
        {
          expiresIn: "22h",
        }
      );
      user.token = token;
      res.status(200).json(user);
    }
    res.status(400);
  } catch (err) {
    res.status(400);
  }
};

module.exports = { regUser, loginUser };