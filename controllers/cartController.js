const Cart = require('../models/cart.js');
const User = require('../models/user.js');
const Product = require('../models/product.js');

exports.createCart = async function (request, response) {
    try {
        let user = await User.findOne({ _id: request.decoded.user_id });
        if (user.cart) {
            response.status(400).send("У вас уже есть одна корзина")
        }
        else {
            const newCart = await new Cart({});
            newCart.user = request.decoded.user_id;
            await newCart.save();
            user.cart = newCart._id;
            await user.save();
            response.send(newCart);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
}

exports.getCartById = async function (request, response) {
    try {
        let user = await User.findOne({_id: request.user.user_id});
        const cart = await Cart.findOne({ _id: user.cart });
        response.json({cart})
    }
    catch (err) {
        response.status(400)
    }
}

exports.addProductToCart = async function (request, response) {
    try {
        let cartUser = await User.findOne({ _id: request.decoded.user_id });
        let cart = await Cart.findOne({ _id: cartUser.cart._id });
        let product = await Product.findById(request.body.id);

        if (!product) {
            response.status(400)
        }
        else if (product && cart) {
            cart.products.push(product._id.toString());
            await cart.save();
            response.send(cart)
        }
        else {
            response.status(400)
        }
    }
    catch (err) {
        response.status(400)
    }
}
