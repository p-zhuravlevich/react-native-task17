const User = require('../models/user');

const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');


const createUser = async (req, res) => {
    try {
        const { name, login, password } = req.body;
    if (!(login && password && name)) {
      res.status(400).send("You can't create new User without required input.");
    }
    const oldUser = await User.findOne({ login });
    if (oldUser) {
      return res.status(409).send("User Already Exist. Please login");
    }
    role = req.body.role;
    encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      name,
      login: login.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    
    const token = jwt.sign(
      { user_id: user._id, login },
        accessToken,
      {
        expiresIn: "2h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch (err) {
    console.log(err);
  }
};

const deleteUser = async (req, res) => {
    try {
        const userIdforDelete = await User.findByIdAndDelete(req.params.id);
        res.send(userIdforDelete);
        }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }   
};

const updateUser = async (req, res) => {
    try {
        const user = await User.findOne({_id: req.decoded.user_id});
        const users = req.body;
        const userIdForUpdate = await User.findByIdAndUpdate(req.decoded.user_id, {
            name: ((users.name != null)?(user.name = users.name):(user.name = user.name)),
            login: ((users.login != null)?(user.login = users.login):(user.login = user.login)),
        })
        res.send(userIdForUpdate);
    }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }    
};

const getUserByID = async (req, res) => {
    try {
            const getUserId= await User.findById(req.params.id); // поиск из одного поля
            res.send(getUserId);
    }
    catch (err) {
        console.log(err);
        res.status(403).send("oh no");
    }
};

const getAllUsers = async (req, res) => {
    try {
            const getUsersAll = await User.find();
            res.send(getUsersAll);
    }
    catch (err) {
        res.status(403).send("oh no");
    }
}

module.exports = { createUser, deleteUser, updateUser, getUserByID, getAllUsers };