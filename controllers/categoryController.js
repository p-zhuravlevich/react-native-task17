const Category = require('../models/category.js');

exports.createCategory = async function (request, response) {
    try {
        let category = await Category.findOne({ name: request.body.name });
        if (category) {
            response.status(400).send("Такая категория уже существует")
        }
        else {
            const newCategory = new Category({ name: request.body.name});
            await newCategory.save();
            response.send(`Новая категория создана: ${newCategory.name}`);
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

exports.deleteCategory = async function (request, response) {
    try {
        let category = await Category.findOne({_id: request.params.id});
        if (category) {
            Category.deleteOne({ _id: request.params.id }, function (err, result) {
                if (err) return response.send(err);
            });
            response.send(`Удалена категория ${category.name}`);
        }
        else {
            response.send("Такой категории нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло нет так")
    }
};

exports.updateCategory = async function (request, response) {
    try {
        let category = await Category.findOne({ _id: request.params.id });
        if (category) {
            Category.updateOne({ _id: request.params.id }, {name:request.body.name}, function (err, result) {
                if (err) return response.send(err);
            });
            let categoryNew = await Category.findOne({ _id: request.params.id });
            let newName = categoryNew.name
            response.send(`Категория ${category.name} изменена на ${newName}`);
        }
        else {
            response.send("Такой категории нет");
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

exports.getСategoryById = async function (request, response) {
    try {
        let category = await  Category.findOne({ _id: request.params.id });
        if (category) {
            response.send(`Категория: ${category.name}`)
        }
        else {
            response.send("Такой категории нет")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};

exports.getAllСategories = async function (request, response) {
    try {
        let categories = await Category.find();
        if (categories) {
            response.send(`${categories}`)
        }
        else {
            response.send("Категорий не существует")
        }
    }
    catch (err) {
        response.status(400).send("Что-то пошло не так")
    }
};
