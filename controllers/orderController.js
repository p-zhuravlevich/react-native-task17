const Order = require("../models/order.js");
const Cart = require("../models/cart.js");
const User = require("../models/user.js");

exports.createOrder = async function (request, response) {
  try {
    let orderNumber = Date.now().toString();
    let order = await Order.findOne({ number: orderNumber });
    let user = await User.findOne({ _id: request.decoded.user_id });
    let cart = await Cart.findOne({ _id: user.cart._id });
    if (order) {
      console.log('sds');
      response.status(400).send("Такой заказ уже существует");
    } else if (!cart) {
      console.log('sds2');
      response.status(400).send("Такой корзины не существует");
    } else {
      const newOrder = new Order({
        number: orderNumber,
        user: request.decoded.user_id,
      });
      for (i = 0; i < cart.products.length; i++) {
        newOrder.products.push(cart.products[i]);
      }
      await newOrder.save();
      user.orders.push(newOrder._id);
      await user.save();
      cart.products = [];
      await cart.save();
      response.send(
        `Заказ сформирован ${newOrder.number}. Покупатель ${user.name}. Товары ${newOrder.products}`
      );
    }
  } catch (err) {
    response.status(400).send("Что-то пошло не так");
  }
};

exports.deleteOrder = async function (request, response) {

  try {
      let user = await User.findOne({ _id: request.decoded.user_id })
      if (user.role !== "admin") {
          let order = await Order.findById({ _id: request.params.id });
          for (let i = 0; i < user.orders.length; i++) {
              if (order._id.toString() == user.orders[i].toString()) {
                  let orderDel = await Order.findOneAndDelete({_id: request.params.id})
                  await User.updateOne({_id: request.decoded.user_id}, { $pull: { orders:  request.params.id } })
                  response.status(400).send(`Заказ ${orderDel.number} удалён`)
              }
              else {
                  response.status(400).send("Такого заказа не существует или он не ваш")
              }
          }
      }
      else {
          let order = await Order.findById({ _id: request.params.id });
          if (!order) {
              response.status(400).send("Такого заказа не существует")
          }
          else {
              await User.updateOne({orders:order._id}, { $pull: { orders:  request.params.id } })
              let orderDel = await Order.findOneAndDelete({ _id: request.params.id })
              response.status(400).send(`Заказ ${orderDel.number} удалён`)
          }
      }
  }
  catch (err) {
      response.status(400).send("Что то пошло не так")
  }
}


exports.getOrderById = async function (request, response) {
  try {
    let order = await Order.findOne({ _id: request.params.id });
    if (order) {
      response.send(`заказ: ${order}`);
    } else {
      response.send("Такого заказа не существует");
    }
  } catch (err) {
    response.status(400).send("Что-то пошло не так");
  }
};

exports.getAllOrders = async function (request, response) {
    try {
        let user = await User.findOne({ _id: request.decoded.user_id })
        let order = user.orders
            response.json({order})
    }
    catch (err) {
        response.status(400)
    }
};
