import * as Yup from 'yup'

export default validationSchemaReg = Yup.object().shape({
    name: Yup.string()
        .label('Name')
        .required('Please enter your name'),
    password: Yup.string()
        .label('Password')
        .required('Please enter your password')
        .min(2, 'Password must have at least 2 characters '),
    login: Yup.string()
        .label('Login')
        .required('Please enter your login'),
})

