import * as Yup from 'yup'


export default validationSchemaLogin = Yup.object().shape({
    password: Yup.string()
        .label('Password')
        .required('Please enter your password')
        .min(2, 'Password must have at least 2 characters '),
    login: Yup.string()
        .label('Login')
        .required('Please enter your login'),
})