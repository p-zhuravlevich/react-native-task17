import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';

import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from '@react-navigation/native';
import StaticPage1 from '../components/ScreenStaticOne';
import StaticPage2 from '../components/ScreenStaticTwo';
import TabNav from './tab.navigator';
import {scheduleRemoveNotification} from '../utils/notification';

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "black",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
};

const Drawer = createDrawerNavigator();

export default function DrawerNavigator(){
  return(
      <Drawer.Navigator screenOptions={screenOptionStyle}>
        <Drawer.Screen name="Main" component={TabNav} options={({ navigation })  => ({
          headerRight: () => (
            <TouchableOpacity onPress={() => (navigation.navigate('Login'), scheduleRemoveNotification())}>
                <Feather
                    name="power"
                    size={30}
                    color='#ffd300'
                    style={{
                    marginRight: 20
                }}/>
            </TouchableOpacity>
        )
        })}/>
        <Drawer.Screen name="Static Page1" component={StaticPage1} options={({ navigation })  => ({
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Feather
                    name="power"
                    size={30}
                    color='#ffd300'
                    style={{
                    marginRight: 20
                }}/>
            </TouchableOpacity>
        )
        })}/>
        <Drawer.Screen name="Static Page2" component={StaticPage2} options={({ navigation })  => ({
          headerRight: () => (
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Feather
                    name="power"
                    size={30}
                    color='#ffd300'
                    style={{
                    marginRight: 20
                }}/>
            </TouchableOpacity>
        )
        })}/>
      </Drawer.Navigator>
  )
}