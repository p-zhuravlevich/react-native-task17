import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import Login from '../components/Login'
import Registration from '../components/Registration'
import DrawerNavigator from './DrawerNavigator';

const screenOptionStyle = {
    headerStyle: {
      backgroundColor: "black",
    },
    headerTintColor: "white",
    headerBackTitle: "Back",
  };

const Stack = createNativeStackNavigator();

const MainStackNavigator = () => {
    return (
      <Stack.Navigator 
        initialRouteName="Login" 
        screenOptions={screenOptionStyle}> 
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Registration" component={Registration} />
            <Stack.Screen name="MainPage" component={DrawerNavigator} options={{headerShown: false}}/>
      </Stack.Navigator>
  )
}

export { MainStackNavigator};