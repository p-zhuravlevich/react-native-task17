import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import {Products} from '../components/Products';
import MainPage from '../components/MainPage';
import Cart from '../components/Cart';
import Order from '../components/Order';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

export default function TabNav() {
  return (
      <Tab.Navigator 
      
      screenOptions={({ route }) => ({
        tabBarShowLabel: false,
        tabBarStyle: {
          position: 'absolute',
          bottom: 10,
          left: 15,
          right: 15,
          elevation: 0,
          backgroundColor: '#d21900',
          borderRadius: 15,
          height: 70,
          
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.00,

            elevation: 24
          
        },
        tabBarIcon: ({ color, size }) => {
          let iconName;

          if (route.name === 'Account') {
            iconName = 'home';
            size = 30
          } else if (route.name === 'Catalog') {
            iconName = 'logo-steam';
            size = 40
          }else if (route.name === 'Cart'){
            iconName = 'basket';
            size = 40
          }else if (route.name === 'Order'){
            iconName = 'logo-buffer';
            size = 30
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'black',
        tabBarInactiveTintColor: '#56501d',
      })}
      >
        <Tab.Screen name="Account" component={MainPage} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="Order" component={Order} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="Cart" component={Cart} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="Catalog" component={Products} initialParams={{params: true}} options={{headerShown: false}}  />
      </Tab.Navigator>
  );
}