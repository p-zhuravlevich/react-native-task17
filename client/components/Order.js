import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { showAllOrders } from '../services/product.service';

export default function Order(route){
  const [orders, setOrder] = useState([]);

  useEffect(() => {
    (async () => {
      const ordersArr = await showAllOrders();
      setOrder(ordersArr.order)
    })()
  }, [route.params]);

    return (
        <View style={styles.container}>
          <ScrollView>
                  <View>
                    <Text style={styles.header}>My orders</Text>
                    <View style={styles.orderBlock}>
            {
              orders.map((product, index) => (
                <View key={product._id + index} style={styles.productBlock}>
                  <Text style={styles.text}>Order number: {index + 1}</Text>
                    {product.products.map((data,index) => (
                      <View  key={data._id + index} style={styles.productBlock}>
                        <Text key={data._id + index} style={styles.text}>
                            Product: {data.name}
                        </Text>
                        <Text style={styles.text}>
                    Category: {data.category.map((de) => de.name)}
                      </Text>
                      </View>
                    ))}
                  
                </View>
              ))
            }
          </View>
                  </View>
          </ScrollView>
            <StatusBar style="auto" />
      </View>
      )
}


const styles = StyleSheet.create({
  header:{
    fontSize: 26,
    margin: 15, 
    textAlign: 'center'
  },
  orderBlock:{
    flex: 3,
    width: '100%'
  },
  productBlock:{
    fontSize: 20,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 15,
    alignItems: 'center',
    padding: 3,
    margin: 10,
    borderBottomColor: 'black',
    borderLeftColor: 'black',
  },
    buttonUp: {
      margin: 35,
      color: '#1E6738'
    },
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "black",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center",
      color: 'black'
    },
    label: {
      margin: 8,
      color: 'white'
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      backgroundColor: 'white',
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      backgroundColor: '#ffd300',
      paddingBottom: 90,
      flex: 1,
    },
    text: {
      fontSize: 20,
      color: 'black'
    }
  });