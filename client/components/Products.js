import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { addProductIntoCart, getAllProducts } from '../services/product.service';
import { ScrollView } from 'react-native-gesture-handler';
import {schedulePushNotification, scheduleRemoveNotification} from '../utils/notification'

export const Products = () => {
  const [products, showProducts] = useState([]);

  useEffect(() => {
    (async () => {
      const productsArr = await getAllProducts();
      showProducts(productsArr.products)
    })()
  }, []);


  const addToCartBtn = async (id) => {
    Alert.alert('Gratz!','This product added to cart!');
    await scheduleRemoveNotification();
    await schedulePushNotification();
    await addProductIntoCart(id)
  }

    return (
      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.header}>Products</Text>
          <View>
            {
              products.map((product, index) => (
                <View key={product._id + index}  style={styles.products} >
                  <Text style={styles.productName}>{product.name}</Text>
                  <Text style={styles.productCategory}>
                    Category: {product.category.map((product) => 
                  product.name)}</Text>
                  <View style={styles.buttonUp}>
                  <Button color='black' title="Add to cart" onPress={()=> addToCartBtn(product._id)} />
                  </View>
                </View>
              ))
            }
          </View>
        </ScrollView>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
  container:{
    backgroundColor: '#ffd300',
    paddingBottom: 90
  },
  buttonUp:{
    marginBottom: 25,
    marginHorizontal: 130,
    color: '#1E6738',
  },
  header:{
    fontSize: 26, 
    marginTop: 15, 
    textAlign: 'center'
  },
  products:{
    margin: 15,
    paddingTop: 5,
    borderColor: "#000",
    borderWidth: 0,
    borderRadius: 15,
    shadowColor: "red",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 5,
    shadowRadius: 5,
    elevation: 5,
      },
  productName:{
    fontSize: 18, 
    fontWeight: '700', 
    marginTop: 15, 
    textAlign: 'center'
  },
  productCategory:{
    fontSize: 16, 
    marginBottom: 15, 
    textAlign: 'center'
  }
  });
  