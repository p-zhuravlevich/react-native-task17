import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from '../utils/context'
import {ReducerFunction, defaultState} from "../reducers/user.reducer";
import { signUp } from '../services/auth.service';

export default function Registration({navigation, route}) {
    const [data, setData] = useState({
      name: "",
      login: "",
      password: "",
    });

    const handleInputChange = (name) => {
      return (value) => {
        const newData = {...data, [name]: value};
        setData(newData)
      }
    }

    const context = useContext(Context);

    const registration = async() =>{
      const allFields = await signUp(data)
      if(allFields){
        context.handleShowUser(data), navigation.navigate('MainPage')
      }
    }


    return (
      <View style={styles.container}>
        <Text style={styles.text}>Please, sign in:</Text>
          <View style={styles.loginBlock}>
            <Text>Login: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('login')}
              value={data.login}
              placeholder="Login here"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('password')}
              value={data.password}
              placeholder="Password"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text>Name: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('name')}
              value={data.name}
              placeholder="Your name"
            />
          </View>
          <View style={styles.buttonUp}>
          <Button style={styles.buttonUp}
            title={(route.params.isInfo? 'Save' : 'Sign up')}
            color='black'
            onPress={registration}
            /></View>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
  buttonUp: {
    margin: 35,
    color: '#1E6738'
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    color: 'black'
  },
  label: {
    margin: 8,
    color: 'white'
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: 'white',
    borderColor: 'rgb(153, 108, 48)'
  },
  container: {
    flex: 5,
    fontSize: 36,
    backgroundColor: '#ffd300',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 26,
    color: 'black'
  }
});
  