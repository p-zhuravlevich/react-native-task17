import { StyleSheet, Text, View, TextInput, Button, Alert, Modal, Pressable } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { showUserCart, orderCreate } from '../services/product.service';
import {scheduleRemoveNotification} from '../utils/notification';

export default function Cart({route}){
    const [products, setProducts] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
        
    useEffect(() => {
      (async () => {
        const productsArr = await showUserCart();
        setProducts(productsArr.cart.products)
      })()
    }, [route.params]); 

    const createNewOrder = async () => {
      scheduleRemoveNotification();
      await orderCreate();
    }

    return (
        <View style={styles.container}>
          <ScrollView>
            <Text style={styles.text}>
                  <View>
                    <Text style={styles.text}>Cart</Text>
                    {
                      products.map((product, index)=> (
                      <View key={product._id + index} style={styles.productBlock}>
                      <Text>
                        {product.name}
                      </Text>
                      </View>
                      ))
                    }
                  </View>
            </Text>
            <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Create a new order?</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => (setModalVisible(!modalVisible), createNewOrder())}
            >
              <Text style={styles.textStyle}>Yes</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => (setModalVisible(!modalVisible))}
            >
              <Text style={styles.textStyle}>No</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyleButton}>Order!</Text>
      </Pressable>
    </View>
          </ScrollView>
            <StatusBar style="auto" />
      </View>
      
      )
}


const styles = StyleSheet.create({
  productBlock:{
    fontSize: 20,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 15,
    alignItems: 'center',
    padding: 7,
    margin: 10,
    borderBottomColor: 'black',
    borderLeftColor: 'black',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#56501d",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "black",
    color: 'white'
  },
  buttonClose: {
    backgroundColor: "black",
    color: 'white'
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center"
  },
  textStyleButton: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    fontSize: 22,
    fontWeight: '700',
    textAlign: "center"
  },
    buttonUp: {
      margin: 35,
      color: '#1E6738'
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center",
      color: 'black'
    },
    label: {
      margin: 8,
      color: 'white'
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      backgroundColor: 'white',
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: '#ffd300',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
      color: 'black'
    }
  });