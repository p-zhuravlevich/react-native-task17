import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function StaticPage2({navigation}) {

    return (
      <View style={styles.container}>
        <Text style={styles.text}>Hello from static page 2</Text>
        <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
  buttonUp: {
    margin: 35,
    color: '#1E6738'
  },
  button: {
    borderRadius: 10,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    color: 'black'
  },
  label: {
    margin: 8,
    color: 'white'
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: 'white',
    borderColor: 'rgb(153, 108, 48)'
  },
  container: {
    flex: 5,
    fontSize: 36,
    backgroundColor: '#ffd300',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 26,
    color: 'black'
  }
});
  