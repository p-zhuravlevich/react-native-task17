import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import Context from '../utils/context'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {schedulePushNotification, scheduleRemoveNotification} from '../utils/notification'


export default function MainPage({navigation, route}) {
    const [login, setLogin] = useState("");
    const [name, setName] = useState("");
    const context = useContext(Context);

    useEffect(() => {
            setLogin(context.userState.login),
            setName(context.userState.name)  
    }, [])

    useEffect(()=>{
      (async () => {
        if(context.userState.productsLength > 0){
          await schedulePushNotification()
        }
      })()
    }, [context.userState.productsLength])

    return (
      <View style={styles.container}>
        <Text style={styles.text}>Your name: {name}</Text>
        <Text style={styles.text}>Your login: {login}</Text>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
  container: {
    flex: 5,
    fontSize: 36,
    backgroundColor: '#ffd300',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    margin: 15,
    fontSize: 26,
    color: 'black'
  }
});
  