import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from '../utils/context'
import { signIn } from '../services/auth.service';
import {schedulePushNotification, scheduleRemoveNotification} from '../utils/notification'

export default function Login({navigation, route}) {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const context = useContext(Context);

  async function logIn(){
    const allLoginFields = await signIn({login, password})
    if(allLoginFields){
      const {name, login} = allLoginFields;
      const productsLength = allLoginFields.cart.products.length;
      context.handleShowUser({login, password, name, productsLength});
      navigation.navigate('MainPage');
      setLogin("");
      setPassword("");
    }
  }

    return (
      <View style={styles.container}>
        <Text style={styles.text}>Please, sign in:</Text>
          <View style={styles.loginBlock}>
            <Text style={{color: 'black'}}>Login: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setLogin}
              value={login}
              placeholder="Login here"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text style={{color: 'black'}}>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              placeholder="Password"
            />
          </View>

          <Button style={styles.button}
            title="Sign in"
            color='black'
            onPress={() => {logIn()}}
            />
          <View style={styles.buttonUp}>
          <Button style={styles.buttonUp} 
            title="Sign up"
            color='black'
            onPress={() => navigation.navigate('Registration')}
            /></View>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
    buttonUp: {
      margin: 35,
      color: '#1E6738'
    },
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "black",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center",
      color: 'black'
    },
    label: {
      margin: 8,
      color: 'white'
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      backgroundColor: 'white',
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: '#ffd300',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
      color: 'black'
    }
  });
  