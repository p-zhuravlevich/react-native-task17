import {SHOW_USER} from "../constants/user.constants";

export const defaultState = {};

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_USER:
            return {
                ...state, ...action.data
            };
        default:
            return state;
    }
};
