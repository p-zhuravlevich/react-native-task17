import { Alert } from 'react-native';
import { getTokenInfo, storeTokenInfo } from '../services/asyncStorage.service'

const URL = "http://192.168.1.122:3000"

export async function getAllProducts() {
    try{
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/product/get/all`, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${tokenInfo.accessToken}`
            }
        });
        const json = await res.json();
        return json;
    }
    catch(e){
        console.log(e);
    }
}

export async function addProductIntoCart(id) {
    try{
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/cart/add`, {
            method: "PUT",
            body: JSON.stringify({id}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenInfo.accessToken}`
            }
        });
        return res;
    }
    catch(e){
        console.log(e);
    }
}

export async function showUserCart(){
    try{
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/cart/mycart`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenInfo.accessToken}`
            }
        });
        const json = res.json();
        return json;
    }
    catch(e){
        console.log(e);
    }
}

export async function orderCreate(){
    try{
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/order/create`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenInfo.accessToken}`
            }
        });
        return res;
    }
    catch(e){
        console.log(e);
    }
}

export async function showAllOrders(){
    try{
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/order/get`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenInfo.accessToken}`
            }
        });
        const json = res.json();
        return json;
    }
    catch(e){
        console.log(e);
    }
}