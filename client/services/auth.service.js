import { Alert } from 'react-native';
import { storeTokenInfo} from './asyncStorage.service';
import validationSchemaReg from '../utils/registerValidator';
import validationSchemaLogin from '../utils/loginValidator';

const URL = "http://192.168.1.122:3000"


  


export async function signUp(data){
    try{
        await validationSchemaReg.validate(data);
        const res = await fetch(`${URL}/register`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if(res.status === 409){
            Alert.alert('Error', `User already exist.`);
            return false
        }
        if(res.status === 400){
            Alert.alert('Error', `All fields is required.`);
            return false
        }
        if (res.status === 201) {
            const json = await res.json();
            storeTokenInfo({
                accessToken: json.token
            });
            return (json)
        } 
    }
    catch(e){
        Alert.alert('Error', e.message);
    }
}    



export async function signIn(data){
    try{
        await validationSchemaLogin.validate(data);
        const res = await fetch(`${URL}/login`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }); 
        if(res.status === 400){
            Alert.alert('Error', `Wrong user.`);
            return false
        }
        if(res.status === 200) {
            const json = await res.json();
            storeTokenInfo({
                accessToken: json.token
            });
            return (json); 
        }
    }
    catch(e){
        Alert.alert('Error', e.message);
    }
}
