const jwt = require('jsonwebtoken');
const accessToken = require('./config');
const User = require('./models/user');
const googleClientId = '858074835463-03l01lh3a6luc8817o7534n1d3kka1j4.apps.googleusercontent.com'
const googleSecret = "PuYPeUN1X0eztgmHpbk8Trtr";
const googleRedirect = 'http://localhost:3000/googleauth';
const {google} = require('googleapis');
const urlParse = require('url-parse');
const queryString = require('query-string');
const axios = require('axios');

const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    const decoded = jwt.verify(token, accessToken);;
    req.user = decoded;
    req.decoded = decoded;
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  return next();
};

const isAdmin = async (req, res, next) => {
    const admin = await User.findOne({_id: req.decoded.user_id});
  if (admin.role !== "Admin") {
    return res.status(403).send("Forbidden. Ask for Admin")
  }
  return next();
}

module.exports = { verifyToken, isAdmin};